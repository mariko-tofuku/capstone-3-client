//折れ線グラフ
import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import { Form, Button, Container } from 'react-bootstrap';
// import LineChart from '../components/LineChart';
import { Line } from 'react-chartjs-2';
import moment from 'moment';
import AppHelper from '../app-helper';


export default function Trend(){

  const [amount, setAmount] = useState(0)
  const [startDate, setStartDate] = useState(moment().subtract(1,'months').format('YYYY-MM-DD'))
  const [endDate, setEndDate] = useState(moment().format('YYYY-MM-DD'))

    const data={
      // labels: 'YYYY-MM-DD',
      datasets: [
        {
          label: 'Balance',//凡例
          data: amount, //Amountの合計
          fill: true,
          backgroundColor: 'rgba(60, 160, 220, 0.3)',
          borderColor: 'rgb(60, 160, 220)',
          borderWidth: 1
        }
      ]
    }
    
    useEffect(() => {

            const options = {
                  method:"POST",
                  headers: { 
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
                    },
                    body: JSON.stringify(
                    {
                        fromDate: startDate,
                        toDate: endDate
                  	})
            }

            fetch(`${ AppHelper.API_URL }/api/users/get-records-by-range`, options)
            .then(data => data.json())
            .then(recordsData => {
                 console.log(recordsData)
              setAmount(recordsData.map(record => record.balanceAfterTransaction))
            })

            // console.log(amount)
      },[])

	return(
		<React.Fragment>
      <Container>
  			<Head>
  				<title>Balance Trend</title>
  			</Head>
  				<h1>Balance Trend</h1>
  			<Line data={data}/>
        </Container>
		</React.Fragment>
		)
}