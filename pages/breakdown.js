//費目のratioを見る円グラフ　色の設定をランダムにする
import React, { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'
import { Pie } from 'react-chartjs-2';
import AppHelper from '../app-helper';
import styles from '../styles/Home.module.css';
import moment from 'moment';
import Head from 'next/head';
import { colorRandomizer } from '../helpers';


export default function Breakdown(){

	const [categoryName, setCategoryName] = useState([])
	const [amount, setAmount] = useState(0)
	const [startDate, setStartDate] = useState(moment().subtract(1,'months').format('YYYY-MM-DD'))//momen().subtract(1, 'months')は１か月前の同日時に変更する
	const [endDate, setEndDate] = useState(moment().format('YYYY-MM-DD'))
	const [bgColors, setBgColors] = useState([])

	const data = {
		labels: categoryName,
		datasets: [
			{
				data: amount,
				backgroundColor: bgColors,
				hoverBackgroundColor: bgColors
			}
		]
	}


		useEffect(() => {
			const options = { //これをuseEffectの上にあったらlocalStorageのエラーがでたのでこの中に。
		      method:"POST",
		      headers: { 
		        'Content-Type' : 'application/json',
		        'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
		      },
		      body: JSON.stringify(
		      {
		      	fromDate: startDate,
		      	toDate: endDate
		      })
	    }
			fetch(`${  AppHelper.API_URL }/api/users/get-records-breakdown-by-range`, options)
		
			.then(AppHelper.toJSON)
			.then(recordsData => {
				setCategoryName(recordsData.map(record => record.categoryName))
				setAmount(recordsData.map(record => record.totalAmount))
				setBgColors(recordsData.map(record => `#${colorRandomizer()}`))
				})
		},[])


	return(
		<React.Fragment>
			<Container>
				<Head>
  					<title>Category Breakdown</title>
  				</Head>

  				<h1>Category Breakdown</h1>
				
					<Pie data={data}/>
			</Container>
		</React.Fragment>
		)

	}