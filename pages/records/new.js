import React, { useState, useEffect } from 'react';
import { Form, Row, Col, Card, Button } from 'react-bootstrap';
import View from '../../components/View';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import Router from 'next/router';//ページ遷移に使う

export default function record(){

  const [categoryType, setCategoryType] = useState("")
  const [categoryName, setCategoryName] = useState("")
  const [amount, setAmount] = useState(0)
  const [description, setDescription] = useState("")
  const [balanceAfterTransaction, setBalanceAfterTransaction] = useState(0)
  const [isActive, setIsActive] = useState(false)

  const [categoryNameOptions, setCategoryNameOptions] = useState([])
  
useEffect(() => {
  console.log()
  if(categoryType !== '' && categoryName !== '' && amount > 0) {
    setIsActive(true)
  }else{
    setIsActive(false)
  }
}, [categoryType, categoryName, amount])

  function newRecord(e){
    e.preventDefault()

    console.log(`${amount} with ${categoryName} of ${categoryType} has been recorded!}` )

    const options = {
      method:"POST",
      headers: { 
        'Content-Type' : 'application/json',
        // カテゴリー追加にはユーザーのAuthorizationがいるので追加。
        'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
      },
      //controllers/users.jsのaddRecordの式を参照すると、配列そのままではなく既存のデータに新規をpushを使って追加していくことがわかる。push後で使われているparams.後categoryNameやtypeNameを使う
      body: JSON.stringify({
        categoryName: categoryName,
        typeName: categoryType,
        amount: amount,
        description: description,
        balanceAfterTransaction: balanceAfterTransaction
      })
    }

    fetch(`${ AppHelper.API_URL }/api/users/add-record`, options)
    .then((data) => data.json())
    .then((data) => {
      console.log(data);
    });

    Swal.fire(
          'Successfully recorded!',
          '',
          'success')
    console.log('New record: ${categoryName} of ${categoryType} with the amount ${amount} was created. ')

    Router.push('../records')　//ページ自動遷移の場合はstateを""にする必要なし。 
  }

// categoryTypeを選ぶとlatestのcategoryNameのリストがoptionに出てくるようにしたい。以下にcategories/index.jsのfetchでcategoriesのデータをとってくる。
  useEffect(() => { 
          const options = {
                method:"POST",
                headers: { 
                  'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
                } 
          }

          fetch(`${ AppHelper.API_URL }/api/users/get-categories`, options)
          .then(data => data.json()) //(AppHelper.toJSON)
          .then(categoryData => {          
          // console.log(categoryData)
                const categoriesRows = categoryData.map(category => {
                      return (
                        <option key={category._id} value={category.name}>{category.name}</option>
                      )
                })
                setCategoryNameOptions(categoriesRows)
          })

    },[])
  //ここまで追加のfetch

	return(
  	<View title={'New Record'}>
      <h3>New Record</h3>
      <Card>
      
        <Card.Header>Record Information</Card.Header>

          <Card.Body>
            <Form onSubmit={e => newRecord(e)}>

              <Form.Group controlId="selectCategoryType">
                <Form.Label>Category Type:</Form.Label>
                <Form.Control as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)}>
                  <option defaultValue>Select category type</option>
                  <option value="Income">Income</option>
                  <option value="Expense">Expense</option>
                </Form.Control>            
                
              </Form.Group>

              <Form.Group controlId="selectCategoryName">
                <Form.Label>Category Name:</Form.Label>
                  <Form.Control as="select" value={categoryName} onChange={e => setCategoryName(e.target.value)}>
                    <option defaultValue>Select category Name</option>
                    {categoryNameOptions}
                  </Form.Control>            
              </Form.Group>

              <Form.Group controlId="amount">
                <Form.Label>Amount:</Form.Label>
                <Form.Control type="number" placeholder="0" value={amount} onChange={e => setAmount(parseFloat(e.target.value))} required/>{/*parseFloatを入れることで、concatになってしまった計算を直した。間違ったdataは削除*/}
                 
              </Form.Group>

              <Form.Group controlId="description">
                <Form.Label>Description:</Form.Label>
                <Form.Control type="text" placeholder="Enter description" value={description} onChange={e => setDescription(e.target.value)} required/>  
              </Form.Group>
              {isActive === true
                ? 
                <Button variant="primary" type="submit">Record!</Button>
                :
                <Button variant="primary" type="submit" disabled>Record!</Button>
              }
            </Form>
          </Card.Body>
          
      </Card>
    </View>
	)
}