module.exports = {
	API_URL: process.env.NEXT_PUBLIC_API_URL,
// 長いNEXT～をタイプするのを避けてAPI_URLのタイプで済ますため 以下も同じ　Variable for our API URL in our .env.local file 　　Node.jsの環境変数は、process.envというオブジェクトに格納される。env fileはここにimportしなくてもよい。
getAccessToken: () => localStorage.getItem('token'), //function for getting our token from localStorage （setItemでlocalStorageに保存されたものをgetItemで取得）
toJSON: (res) => res.json() // for converting response to json format
} 