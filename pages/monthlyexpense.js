import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import { Form, Button, Container } from 'react-bootstrap';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';
import AppHelper from '../app-helper';

export default function MonthlyExpense(){

	const [months, setMonths] = useState([])
	const [monthlyExpense, setMonthlyExpense] = useState([])
	const [startDate, setStartDate] = useState(moment().subtract(1,'years').format('YYYY-MM-DD'))
  const [endDate, setEndDate] = useState(moment().format('YYYY-MM-DD'))
  const [amount, setAmount] = ([])

    const data={
      labels: months, //X軸
      datasets: [
        {
          label: 'Monthly Expense',//凡例
          data: monthlyExpense, //Y軸
          backgroundColor: 'rgba(255, 99, 132, 0.2)',
          borderColor: 'rgba(255, 99, 132, 0.2)',
          hoverBackgroundColor: 'rgba(255, 99, 132, 0.2)',
          hoverBorderColor: 'rgba(255, 99, 132, 0.2)'
        }
      ]
    }
    
    useEffect(() => {

            const options = {
                  method:"POST",
                  headers: { 
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
                    },
                  body: JSON.stringify(
                  {
                        fromDate: startDate,
                        toDate: endDate
                  	})
            }

            fetch(`${ AppHelper.API_URL }/api/users/get-records-by-range`, options)
            .then(data => data.json())
            .then(recordsData => {
              // console.log(recordsData)
              let tempMonths = [];
              recordsData.forEach(element => {
              	if(!tempMonths.find(month => month === moment(element.dateAdded).format('MMMM'))){
              		tempMonths.push(moment(element.dateAdded).format('MMMM'))
              	}
              })
              // console.log(tempMonths)
              const monthRef = ['January', "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
              tempMonths.sort((a,b) => {
              	if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){
              		return monthsRef.indexOf(a) - monthsRef.indexOf(b)
              	}

              })
              setMonths(tempMonths)
              // console.log(months) Aprilがでてきている
            })
            
      },[])

    useEffect(() => {
            
            const options = {
                  method:"POST",
                  headers: { 
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
                    },
                    body: JSON.stringify(
                    {
                        fromDate: startDate,
                        toDate: endDate
                  	})
                 }

            fetch(`${ AppHelper.API_URL }/api/users/get-records-by-range`, options)
            .then(data => data.json())
            .then(recordsData => {
              // console.log(recordsData)
            	
              
            	let expenseOnly = recordsData.filter(element => element.type == 'Expense')
              // console.log(expenseOnly)

              setMonthlyExpense(months.map(month => {

            	expenseOnly.forEach(element => {
            		if(moment(element.dateAdded).format('MMMM') === month){

                  let amount = 0
            			amount = amount + parseInt(element.amount)
                  // console.log(amount)
            		}

            	})

              return amount


            }))
              // console.log(monthlyExpense)

        })

      },[months])

// console.log(monthlyExpense)

	return(
		<React.Fragment>
      <Container>
  			<Head>
  				<title>Monthly Expense</title>
  			</Head>
  				<h1>Monthly Expense</h1>
  			<Bar data={data}/>
        </Container>
		</React.Fragment>
		)
}