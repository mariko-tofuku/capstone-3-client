import React, {useState, useEffect} from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import Head from 'next/head';
import View from '../../components/View';
import Swal from 'sweetalert2';
import Router from 'next/router';
import AppHelper from '../../app-helper';

export default function Register(){

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [mobileNo, setMobileNo] = useState(0)
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState("")
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true)

		}else{
			setIsActive(false)
		}
	}, [email, password1, password2])

	function registerUser(e){
		e.preventDefault()

		console.log(`User ${email} successfully registered with password: ${password1}`)
		
		// add fetch request - addition from here
		const options = {
			method:"POST",
			headers: { 'Content-Type' : 'application/json'},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password1
			})
		}

		fetch(`${ AppHelper.API_URL }/api/users/register`, options)
		.then((data) => data.json())
		.then((data) => {
			console.log(data);
		});
		// add fetch request - addition from line33 to here

		//add
		setFirstName("")
		setLastName("")
		setMobileNo(0)
		//オリジナル
		setEmail("")
		setPassword1("")
		setPassword2("")

		Swal.fire(
					'You are successfully registered!',
					'Thank you!',
					'success')
		Router.push('login')//registerした後にlogin画面へ遷移させる
		// alert("Thank you for registering!")
	}

	return(
		<View title={"User Registration"}>
		
				<Form onSubmit={e => registerUser(e)}>

					<Form.Group controlId="userFirstName">
						<Form.Label>First Name</Form.Label>
						<Form.Control type="text" placeholder="Enter your First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="userLastName">
						<Form.Label>Last Name</Form.Label>
						<Form.Control type="text" placeholder="Enter your Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
					</Form.Group>


					<Form.Group controlId="userEmail">
						<Form.Label>Email</Form.Label>
						<Form.Control type="email" placeholder="Enter your email" value={email} onChange={e => setEmail(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="mobileNumber">
						<Form.Label>Mobile Number</Form.Label> {/*typeはnumberかtextか？*/}
						<Form.Control type="number" placeholder="Enter Mobile Number" value={mobileNo} onChange={e => setMobileNo(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="password1">
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Enter Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="password2">
						<Form.Label>Confirm Password</Form.Label>
						<Form.Control type="password" placeholder="Confirm Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
					</Form.Group>

					{isActive === true
						? <Button variant="primary" type="submit" id="submitBtn">Register</Button>
						: <Button variant="primary" type="submit" id="submitBtn" disabled>Register</Button>
					}
					
				</Form>
		</View>	
	)
}