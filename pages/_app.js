import '../styles/globals.css'
import React, { useState, useEffect } from 'react';
import NavBar from '../components/NavBar';
// import { Container } from 'react-bootstrap';
// import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootswatch/dist/minty/bootstrap.min.css';
import '../styles/globals.css'
import { UserProvider } from '../UserContext';
import AppHelper from '../app-helper';


function MyApp({ Component, pageProps }) {
	const [user, setUser] = useState({
		id: null
	})

	useEffect(() => {
		const options = {
			headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }`}
		}

		fetch(`${ AppHelper.API_URL }/api/users/details`, options)
		.then(AppHelper.toJSON)
		.then(data => {

			if(typeof data._id !== 'undefined'){
				setUser({ id: data._id }) //今回Adminはないのではずしてidのみに
			}else{
				setUser({ id: null })//今回Adminはないのではずしてidのみに
			}
		})

	}, [])

	const unsetUser = () => {
		localStorage.clear()

		setUser({
			id: null//今回Adminはないのではずしてidのみに
		})
	}

  return( 
	  	<UserProvider value={{user, setUser, unsetUser}}>
	  		<NavBar/>
	  			<Component {...pageProps} />
	  	</UserProvider>
	)
}

export default MyApp
