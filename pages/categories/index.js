import React, { useState, useEffect } from 'react';
import { Container, Button, Table } from 'react-bootstrap';
import View from '../../components/View';
import AppHelper from '../../app-helper'
import Head from 'next/head';

export default function Categories(){

      const [categories, setCategories] = useState([])

    useEffect(() => { //ここでuseEffectを使う理由。一つでもStateが変わる（この場合、空欄の配列useState([])に入力する）とその度に作動(fire)。useEffectを使うことで、userが他のページに行って戻て来たとしても。入力した内容が変わるごとに実行される。これを使わないと実行は一度きり。

            const options = {
                  method:"POST",
                  headers: { 
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
                        } 
                  }

            fetch(`${ AppHelper.API_URL }/api/users/get-categories`, options)
            .then(data => data.json()) //(data => data.json())
            .then(categoryData => {          
            // console.log(categoryData)
                  const categoriesRows = categoryData.map(category => {
                        return (
                              <tr key={category._id}>
                                    <td>{category.name}</td>
                                    <td>{category.type}</td>
                                    <td>
                                          <Button variant="warning" href="/categories/update">Update</Button>
                                          <Button variant="danger"  href="/categories/delete">Delete</Button>
                                    </td>
                              </tr>
                        )
                  })
                  setCategories(categoriesRows)
            })

      },[])

    return(
      <View title={'Categories'}>
            <h3>Categories</h3>
            <Button variant="success" href="/categories/new">Add</Button>
                  <Table>
                        <thead>
                              <tr>
                                    <th>Category</th>
                                    <th>Type</th>
                                    <th>Update/Delete</th>
                              </tr>
                        </thead>
                        <tbody>
                              {categories}                         
                        </tbody>
                  </Table>
      </View>
      )
}
