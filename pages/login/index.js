import { Row, Col } from 'react-bootstrap';
import LoginForm from '../../components/LoginForm';
import View from '../../components/View';

export default function Login(){
	return(
  	<View title={'Budget Tracking Login'}>
        <Row className="justify-content-center">
          <Col xs md="6">
            <h2>Zuitt Budget Tracking</h2>
            <h5>Log In</h5>
            <LoginForm/>
            <p className = "text-center mr-2">If you haven't registered yet, <a role="button" href="/register" >Sign Up Here!</a>
            </p>
          </Col>
        </Row>
      </View>
	)
}