import React, { useContext } from 'react';
import Link from 'next/link'; 
import { Navbar, Nav } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function NavBar() {

	const { user } = useContext(UserContext)

	return (
		 <Navbar bg="primary" expand="lg" className="text-white">
			<Link href="/"> 
				<a className="navbar-brand">Budget Tracker</a> 
			</Link> 
			<Navbar.Toggle aria-controls="basic-navbar-nav"/> 
			<Navbar.Collapse id="basic-navbar-nav"> 
				<Nav className="mr-auto">
					<Link href="/categories"> 
						<a className="nav-link" role="button">Categories</a> 
					</Link> 
					<Link href="/records"> 
						<a className="nav-link" role="button">Records</a> 
					</Link> 
					<Link href="/monthlyexpense"> 
						<a className="nav-link" role="button">Monthly Expense</a> 
					</Link> 
					<Link href="/monthlyincome"> 
						<a className="nav-link" role="button">Monthly Income</a> 
					</Link> 
					<Link href="/trend"> 
						<a className="nav-link" role="button">Trend</a>
					</Link>
					<Link href="/breakdown"> 
						<a className="nav-link" role="button">Breakdown</a>
					</Link>
				</Nav>
				{user.id !== null
					?
				<React.Fragment>
				<Nav className="ml-auto">{/*ml-auto中央に余白*/}
					<Link href="/logout"> 
						<a className="nav-link" role="button">Logout</a> 
					</Link> 
				</Nav>
				</React.Fragment>
					:
					<React.Fragment>
						<Nav className="ml-auto">{/*ml-auto中央に余白*/}
							<Link href="/login"> 
								<a className="nav-link" role="button">Login</a> 
							</Link> 
						</Nav>
					</React.Fragment>
					} 
			</Navbar.Collapse> 
		</Navbar> 
	) 
}
