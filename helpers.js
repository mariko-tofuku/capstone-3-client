const colorRandomizer = () => {
	return Math.floor(Math.random()*16777215).toString(16)
}
//Math.floorは小数点以下切り捨て（小さい整数へ）Math.randomは乱数作成。Math.random()*数字で、数字は最大値。hexdecimal color 16進法の文字列にする toString(16) 16777215は16進数ではffffffで色を表す最大 その上の数に色はない
export { colorRandomizer }