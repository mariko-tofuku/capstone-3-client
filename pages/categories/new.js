import React, { useState, useEffect } from 'react';
import { Form, Row, Col, Card, Button } from 'react-bootstrap';
import View from '../../components/View';
import Swal from 'sweetalert2';
import Router from 'next/router';
import AppHelper from '../../app-helper';

export default function Create(){

  const [categoryName, setCategoryName] = useState("")
  const [categoryType, setCategoryType] = useState("") //ここのデフォルト
  const [isActive, setIsActive] = useState(false)

  useEffect(() => {
    if(categoryName !== '' && categoryType !== ''){
      setIsActive(true)

    }else{
      setIsActive(false)
    }
  }, [categoryName, categoryType])

  function createCategoryName(e){
    e.preventDefault()

    console.log(`${categoryName} has been created with the ${categoryType}` )
    //
    const options = {
      method:"POST",
      headers: { 
        'Content-Type' : 'application/json',
        // カテゴリー追加にはユーザーのAuthorizationがいるので追加。
        'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
      },
      // models/user.jsを見てcategories以下の配列を追加しようとしていた。そうではなく、controllers/users.jsのaddCategoryの式を参照すると、配列そのままではなく既存のデータに新規をpushを使って追加していくことがわかる。そこで使われているparams.xxxのxxx部分nameとtypeNameを使う
      body: JSON.stringify({
        name: categoryName,
        typeName: categoryType
      })
    }

    fetch(`${ AppHelper.API_URL }/api/users/add-category`, options)
    .then((data) => data.json())
    .then((data) => {
      console.log(data);
    });
    //

    setCategoryName("")
    setCategoryType("")

    Swal.fire(
          'New category is created!',
          '',
          'success')
    Router.push('../categories')
  }

	return(
  	<View title={'New Category'}>
      <h3>New Category</h3>
      <Card>
        <Card.Header>Category Information</Card.Header>

          <Card.Body>
            <Form onSubmit={e => createCategoryName(e)}>

              <Form.Group controlId="addCategory">
                <Form.Label>Category Name:</Form.Label>
                <Form.Control type="text" placeholder="Enter new category name" value={categoryName} onChange={e => setCategoryName(e.target.value)} required/>
              </Form.Group>

              <Form.Group controlId="chooseCategoryType">
                <Form.Label>Category Type:</Form.Label>
                  <Form.Control as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)}>
                    <option selected>Select Category</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                  </Form.Control>            
              </Form.Group>
              {isActive === true
                ?
                <Button variant="primary" type="submit">Submit</Button>
                :
                <Button variant="primary" type="submit" disabled>Submit</Button>
              }
            </Form>
          </Card.Body>

      </Card>
    </View>
	)
}