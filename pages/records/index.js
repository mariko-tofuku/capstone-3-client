import { Button, Table, Container, Form, Card, Col, Row } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import View from '../../components/View';
import AppHelper from '../../app-helper';
import Head from 'next/head';
import moment from 'moment';

export default function Records(){

      const[showRecords, setShowRecords] = useState([])
      // const[showSearchRecords,setShowSearchRecords] = useState([])
      const[searchKeyword, setSearchKeyword] = useState('')
      const[searchType, setSearchType] = useState('All')
      //ここでuseEffectを使う理由。一つでもStateが変わる（この場合、空欄の配列useState([])に入力する）とその度に作動(fire)。useEffectを使うことで、userが他のページに行って戻て来たとしても。入力した内容が変わるごとに実行される。これを使わないと実行は一度きり。
      function recentRecords(){
        const options = {
                  method:"POST",
                  headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
                  }
            }

            fetch(`${ AppHelper.API_URL }/api/users/get-most-recent-records`, options)

            .then(data => data.json())
            .then(recordData => {          
                  const recordsRows = recordData.map(record => {
                        return (
                          <Card>
                            <Card.Body key={record._id}>
                              <Card.Title value={record.categoryName}>{record.categoryName}</Card.Title>
                              <Card.Text>
                                <span value={record.description}>Description: {record.description}</span>
                                <br/>
                                <span value={record.type}>({record.type})</span>
                                <br/>
                                <span value={record.amount}>Amount: {record.amount}</span>
                                <br/>
                                <span value={record.balanceAfterTransaction}>Latest balance: {record.balanceAfterTransaction}</span>
                                <br/>
                                <span value={record.dateAdded}>Date: {moment(record.dateAdded).format('LL')}</span>
                              </Card.Text>
                                <Button variant="warning" href="/records/update">Update</Button>
                                <Button variant="danger" href="/records/delete">Delete</Button>
                            </Card.Body>
                          </Card>
                        )
                  })
                  setShowRecords(recordsRows)//mapの外
            })
      }
          
        function searchRecords(){
        const options = {
                  method:"POST",
                  headers: { 
                     'Content-Type': 'application/json',
                    'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
                  },
                  body: JSON.stringify({
                        searchKeyword: searchKeyword,
                        searchType: searchType
                  })
            }
            fetch(`${ AppHelper.API_URL }/api/users/search-record`, options)
            .then(data => data.json()) 
            .then(recordData => {          
            // console.log(recordData)
                  const recordsRows = recordData.map(record => {
                        return (
                          <Card>
                            <Col>
                            <Card.Body key={record._id}>
                              <Card.Title value={record.categoryName}>{record.categoryName}</Card.Title>
                              <Card.Text>
                                <span value={record.description}>Description: {record.description}</span>
                                <br/>
                                <span value={record.type}>Category type: {record.type}</span>
                                <br/>
                                <span value={record.amount}>Amount: {record.amount}</span>
                                <br/>
                                <span value={record.balanceAfterTransaction}>Latest balance: {record.balanceAfterTransaction}</span>
                                <br/>
                                <span value={record.dateAdded}>Date: {moment(record.dateAdded).format('LL')}</span>
                              </Card.Text>

                                <Button variant="warning" href="/records/update">Update</Button>
                                <Button variant="danger" href="/records/delete">Delete</Button>

                            </Card.Body>
                            </Col>
                          </Card>
                        )
                  })
                  setShowRecords(recordsRows)//mapの外
            })

      }  


//useEffect
      useEffect (() => {
        if (searchKeyword !== '' || searchType !== 'All'){
          searchRecords()
        } else {
          recentRecords()
        }
      },[searchKeyword, searchType])


      return(
        <React.Fragment>
          <Container>
            <h1>Records</h1>
            <br/>
            <Button variant="success" href="/records/new">Add</Button>
            <br/>
            
              <Form.Control type="text" placeholder="Search Record" value={searchKeyword} onChange={e=> setSearchKeyword(e.target.value)}/>
              <br/>
           

            
              <Form.Group>
                <Form.Label>Select Category Type</Form.Label>
                  <Form.Control as="select" value={searchType} onChange={e => setSearchType(e.target.value)}>
                    <option selected value='All'>All</option>
                    <option value="Income"> Income</option>
                    <option value="Expense">Expense</option>
                  </Form.Control>
              </Form.Group>        
            
            {showRecords}
          </Container>
        </React.Fragment>
    
      )
}