import React, { useState, useContext } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Router from 'next/router';
import Swal from 'sweetalert2';
import AppHelper from '../app-helper';
import UserContext from '../UserContext';

export default function LoginForm(){
	const { setUser } = useContext(UserContext)
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")

	function authenticate(e){
		e.preventDefault()

		const options = {
			method: 'POST',　
			headers: { 'Content-Type' : 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password//authenticateにはemailとpasswordでよい。registerとは違う。
			})
		}

		fetch(`${ AppHelper.API_URL }/api/users/login`, options)

		.then(AppHelper.toJSON)
		.then(data => {
			//console.log(data) backendからのresponseをここで見る
			if(typeof data.access !== 'undefined'){

				
				Swal.fire(
					'Success',
					'You have successfully logged in. ',
					'success'
				)
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

			} else {

				if(data.error === "does-not-exist"){
					Swal.fire(
						'Authentication failed',
						'User does not exist.',
						'error'
					)
				}else if(data.error === "incorrect-password"){
					Swal.fire(
						'Authentication failed',
						'Password is incorrect.',
						'error'
						)

				}else if(data.error === "login-type-error"){
					Swal.fire(
						'Authentication failed',
						'You may have registered through a different login procedure.',
						'error'
					)
				}
			}
		})
		
	}

	function authenticateGoogleToken(response){
		// console.log(response) //console.log check
		const payload = {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({tokenId: response.tokenId })//
		}

		fetch(`${ AppHelper.API_URL }/api/users/verify-google-id-token`, payload)//backend/routes/user.js line6 /verify-google-id-token　& backend/index.id api/users　確認済み
		.then(AppHelper.toJSON)
		.then(data => {
	
			if(typeof data.access !== 'undefined'){
			
				Swal.fire(
					'Success',
					'You have successfully logged in. ',
					'success'
				)

				localStorage.setItem('token', data.access)
				localStorage.setItem('googleToken', response.accessToken)
				retrieveUserDetails(data.access)
			} else {
				if(data.error === "google-auth-error"){
					Swal.fire(
					'Authentication Failed',
					'Google authentication procedure failed. ',
					'error'
				)

				}else if(data.error === "login-type-error"){
					Swal.fire(
					'Authentication Failed',
					'You may have registered throught a different login procedure. ',
					'error'
				)
			}

		}

	})
}

	function retrieveUserDetails(accessToken){
		const options = {
			headers: { Authorization: `Bearer ${ accessToken }`}
		}

		fetch(`${ AppHelper.API_URL }/api/users/details`, options)//backend/routes/user.js line20 /details　& backend/index.id api/users　確認済み
		.then(AppHelper.toJSON)
		.then(data => {
			
			setUser({ id: data._id })//

			Router.push('records')// login後に遷移させたいエンドポイントはRecords。
		})
	}

	return(
		<Card>
			<Card.Header>Login</Card.Header>
			<Card.Body>
				<Form onSubmit={e => authenticate(e)}>

					<Form.Group controlId="userEmail">
						<Form.Label>Email:</Form.Label>
						<Form.Control type="email" placeholder="Enter your email" value={email} onChange={e => setEmail(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="password">
						<Form.Label>Password:</Form.Label>
						<Form.Control type="password" placeholder="Enter your password" value={password} onChange={e => setPassword(e.target.value)} required/>
					</Form.Group>

					<Button variant="primary" type="submit" block>Email Log In</Button> {/*blockの意味はfull width of parent https://react-bootstrap.github.io/components/buttons*/}
					<GoogleLogin
						clientId="382092790122-8nhbviqkhdtm2gr7p64j1rkcs6d87jho.apps.googleusercontent.com"
						buttonText="Google Log In"// テキストは自由に変えられる
						onSuccess={authenticateGoogleToken}
						onFailure={authenticateGoogleToken}
						cookiePolicy={'single_host_origin'}
						className="w-100 text-center d-flex justify-content-center"//ボタンの大きさなど変えられる
					/>
				</Form>
			</Card.Body>
		</Card>	
	)
}